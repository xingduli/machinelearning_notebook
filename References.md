# References
可以自行在下属列表找找到适合自己的学习资料，虽然罗列的比较多，但是个人最好选择一个深入阅读、练习。当练习到一定程度，可以再看看其他的资料，这样弥补单一学习资料可能存在的欠缺。

列表等在 https://gitee.com/pi-lab/pilab_research_fields/blob/master/references/ML_References.md



## References

* [形象直观了解谷歌大脑新型优化器LAMB](https://www.toutiao.com/i6687162064395305475/)
* [梯度下降方法的视觉解释（动量，AdaGrad，RMSProp，Adam）](https://www.toutiao.com/i6836422484028293640/)
* [5种常用的交叉验证技术，保证评估模型的稳定性](https://m.toutiaocdn.com/i6838062457596936718)
* [The Neural network zoo](https://www.asimovinstitute.org/neural-network-zoo/) : 各种神经网络架构的解释、图示
* 22 个神经网络结构设计/可视化工具
	- https://www.toutiao.com/i6836884346155041292/
- https://github.com/ashishpatel26/Tools-to-Design-or-Visualize-Architecture-of-Neural-Network
	
* CNN 可视化工具 https://m.toutiaocdn.com/group/6822123587156050435
	- https://poloclub.github.io/cnn-explainer/
	- https://github.com/poloclub/cnn-explainer

* 打标签工具
	- [Label Studio](https://labelstud.io/)
		- Demo video https://www.bilibili.com/video/BV1dL41147KE
		- Documents https://labelstud.io/guide/
	- [LabelImg](https://github.com/tzutalin/labelImg)

* 一款图像转卡通的Python项目，超级值得你练手
	- https://www.toutiao.com/a6821299115175969287/
	- https://github.com/minivision-ai/photo2cartoon

* [Awesome Deep Learning Project Ideas](https://github.com/NirantK/awesome-project-ideas)

* [Machine Learning From Scratch](https://github.com/eriklindernoren/ML-From-Scratch)



## Course & Code
* [《统计学习方法》的代码](https://gitee.com/afishoutis/MachineLearning)

## Exercise
* http://sofasofa.io/competitions.php?type=practice
* https://www.kaggle.com/competitions
* Machine learning project ideas
  * https://data-flair.training/blogs/machine-learning-project-ideas/
  * https://data-flair.training/blogs/deep-learning-project-ideas/
  * https://www.kdnuggets.com/2020/03/20-machine-learning-datasets-project-ideas.html


* Titanic: notebooks/data-science-ipython-notebooks/kaggle/titanic.ipynb
* 使用神经网络解决拼图游戏 https://www.toutiao.com/a6855437347463365133/
* [Sudoku-Solver](https://github.com/shivaverma/Sudoku-Solver)


## Method

* Programming Multiclass Logistic Regression
notebooks/MachineLearningNotebooks/05.%20Logistic%20Regression.ipynb

* Equation for MLP
notebooks/MachineLearningNotebooks/07.%20MLP%20Neural%20Networks.ipynb

* Optimization methods
notebooks/MachineLearningNotebooks/06.%20Optimization.ipynb


* https://github.com/wmpscc/DataMiningNotesAndPractice/blob/master/2.KMeans%E7%AE%97%E6%B3%95%E4%B8%8E%E4%BA%A4%E9%80%9A%E4%BA%8B%E6%95%85%E7%90%86%E8%B5%94%E5%AE%A1%E6%A0%B8%E9%A2%84%E6%B5%8B.md

* evaluation metrics
http://localhost:8889/notebooks/machineLearning/10_digits_classification.ipynb


* model selection and assessment
http://localhost:8889/notebooks/machineLearning/notebooks/01%20-%20Model%20Selection%20and%20Assessment.ipynb


## NN
* 神经网络——梯度下降&反向传播 https://blog.csdn.net/skullfang/article/details/78634317
* 零基础入门深度学习(3) - 神经网络和反向传播算法 https://www.zybuluo.com/hanbingtao/note/476663
* 如何直观地解释 backpropagation 算法？ https://www.zhihu.com/question/27239198
* 一文弄懂神经网络中的反向传播法——BackPropagation https://www.cnblogs.com/charlotte77/p/5629865.html

* https://medium.com/@UdacityINDIA/how-to-build-your-first-neural-network-with-python-6819c7f65dbf
* https://enlight.nyc/projects/neural-network/
* https://www.python-course.eu/neural_networks_with_python_numpy.php


## k-Means
* [如何使用 Keras 实现无监督聚类](http://m.sohu.com/a/236221126_717210)

## AutoEncoder (自编码/非监督学习)
* https://morvanzhou.github.io/tutorials/machine-learning/torch/4-04-autoencoder/
* https://github.com/MorvanZhou/PyTorch-Tutorial/blob/master/tutorial-contents/404_autoencoder.py
* pytorch AutoEncoder 自编码 https://www.jianshu.com/p/f0929f427d03
* Adversarial Autoencoders (with Pytorch) https://blog.paperspace.com/adversarial-autoencoders-with-pytorch/