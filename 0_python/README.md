
# 简明Python教程 （90分钟学会Python）

Python 是一门上手简单、功能强大、通用型的脚本编程语言。

* Python 类库极其丰富，这使得 Python 几乎无所不能，网站开发、软件开发、大数据分析、网络爬虫、机器学习等都不在话下。
* Python最主要的优点是使用人类的思考方式来编写程序，大多数情况下使用封装好的库能够快速完成给定的任务，虽然执行的效率不一定很高，但是极大的缩短了程序设计、编写、调试的时间，因此非常适合快速学习、尝试、试错。

关于Python的安装可以参考[《安装Python环境》](../references_tips/InstallPython.md)，或者自行去网络上查找相关的资料。

![learn python](images/learn_python.jpg)

## 内容
0. [Install Python](../references_tips/InstallPython.md)
1. [IPython & Jupyter Notebook](0-ipython_notebook.ipynb)
2. [Basics](1_Basics.ipynb)
    - Why Python, Zen of Python
    - Variables, Operators, Built-in functions
3. [Print statement](2_Print_Statement.ipynb)
    - Tips of print
4. [Data structure - 1](3_Data_Structure_1.ipynb)
    - Lists, Tuples, Sets
5. [Data structure - 2](4_Data_Structure_2.ipynb)
    - Strings, Dictionaries
6. [Control flow](5_Control_Flow.ipynb)
    - if, else, elif, for, while, break, continue
7. [Functions](6_Function.ipynb)
    - Function define, return, arguments
    - Gloabl and local variables
    - Lambda functions
8. [Class](7_Class.ipynb)
    - Class define
    - Inheritance


## 参考资料
* [安装Python环境](../references_tips/InstallPython.md)
* [IPython Notebooks to learn Python](https://github.com/rajathkmp/Python-Lectures)
* [廖雪峰的Python教程](https://www.liaoxuefeng.com/wiki/1016959663602400)
* [跟海龟学Python](https://gitee.com/pi-lab/python_turtle)
* [智能系统实验室入门教程-Python](https://gitee.com/pi-lab/SummerCamp/tree/master/python)
* [Python Tips](../references_tips/python)
* [Get Started with Python](Python.pdf)
* [Python - 100天从新手到大师](https://github.com/jackfrued/Python-100-Days)